﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class Login : Form
    {
        List<string> listKode = new List<string>();
        List<string> listNama = new List<string>();
        List<string> listEmail = new List<string>();
        List<string> listPassword = new List<string>();
        List<string> listTipe = new List<string>();
        
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            aturWidget();
        }
        public void aturWidget()
        {
            this.Location = new Point(0, 0);
            textBox1.Size = new Size(300, 45);
            textBox2.Size = new Size(300, 45);
            textBox1.Font = new Font("arial", 20);
            textBox2.Font = new Font("arial", 20);
        }

        private void button1_Click(object sender, EventArgs e) //button login
        {
            OracleConnection conn = new OracleConnection("Data Source=sby;User Id=projectdd;password=123");
            //LOAD SISWA
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select * from siswa", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select * from siswa@pucukecabang", conn);
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listKode.Add(reader["kode_siswa"].ToString());
                listNama.Add(reader["nama"].ToString());
                listEmail.Add(reader["email"].ToString());
                listPassword.Add(reader["password"].ToString());
                listTipe.Add("siswa");
            }
            conn.Close();

            //LOAD PEGAWAI SBY(LOKAL)
            conn.Open();
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select * from pegawai", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select * from pegawai@pucukecabang", conn);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listKode.Add(reader["kode_pegawai"].ToString());
                listNama.Add(reader["nama"].ToString());
                listEmail.Add(reader["email"].ToString());
                listPassword.Add(reader["password"].ToString());
                listTipe.Add("pegawaipusat");
            }
            conn.Close();

            //LOAD PEGAWAI DPS(CABANG)
            conn.Open();
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select * from pegawai@pucukecabang", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select * from pegawai", conn);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listKode.Add(reader["kode_pegawai"].ToString());
                listNama.Add(reader["nama"].ToString());
                listEmail.Add(reader["email"].ToString());
                listPassword.Add(reader["password"].ToString());
                listTipe.Add("pegawaicabang");
            }
            conn.Close();

            //cek inputan dengan database
            cekAccount();
        }

        public void cekAccount() {
            bool cek = false;
            for (int i = 0; i < listEmail.Count; i++)
            {
                if (listEmail[i] == textBox1.Text && listPassword[i] == textBox2.Text) {
                    cek = true;
                    if (listTipe[i] == "pegawaicabang")
                    {
                        FormParent f = new FormParent();
                        ((FormParent)MdiParent).kode_accActive = listKode[i];
                        ((FormParent)MdiParent).nama_accActive = listNama[i];
                        ((FormParent)MdiParent).tipe_accActive = "pegawai";
                        ((FormParent)MdiParent).lokasi_accActive = "cabang";
                        
                        ((FormParent)MdiParent).openPegawai();
                        this.Close();
                    }
                    else if (listTipe[i] == "pegawaipusat")
                    {
                        FormParent f = new FormParent();
                        ((FormParent)MdiParent).kode_accActive = listKode[i];
                        ((FormParent)MdiParent).nama_accActive = listNama[i];
                        ((FormParent)MdiParent).tipe_accActive = "pegawai";
                        ((FormParent)MdiParent).lokasi_accActive = "pusat";
                        
                        ((FormParent)MdiParent).openPegawai();
                        this.Close();
                    }
                    else if(listTipe[i] == "siswa"){
                        FormParent f = new FormParent();
                        ((FormParent)MdiParent).kode_accActive = listKode[i];
                        ((FormParent)MdiParent).nama_accActive = listNama[i];
                        ((FormParent)MdiParent).tipe_accActive = "siswa";
                        ((FormParent)MdiParent).lokasi_accActive = "";
                        ((FormParent)MdiParent).openSiswa();
                        this.Close();
                    }
                }
            }
            if (!cek) MessageBox.Show("Username / Password invalid!!!");
        }
    }
}
