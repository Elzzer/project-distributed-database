﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class InsertSoal : Form
    {
        OracleConnection conn;
        String username = "projectdd";
        String password = "123";
        String DB = "SBY";
        String oradb;

        List<string> list_kelas_yang_kosong = new List<string>();

        List<string> listkode = new List<string>();
        List<string> listmapel = new List<string>();
        List<string> listgrade = new List<string>();
        List<string> listjadwal = new List<string>();


        List<string> listsoal = new List<string>();
        List<string> listkelas = new List<string>();
        List<string> listmateri = new List<string>();
        List<string> listpegawai = new List<string>();
        public InsertSoal()
        {
            InitializeComponent();
        }

        private void InsertSoal_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            oradb = "Data Source=" + DB + ";User ID=" + username + ";Password=" + password;
            refresh();
        }

        public void refresh()
        {
            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;

            OracleDataAdapter adpt2 = new OracleDataAdapter();
            OracleCommand cmd2 = new OracleCommand();
            cmd2.Connection = conn;

            OracleDataAdapter adpt3 = new OracleDataAdapter();
            OracleCommand cmd3 = new OracleCommand();
            cmd3.Connection = conn;

            OracleDataAdapter adpt4 = new OracleDataAdapter();
            OracleCommand cmd4 = new OracleCommand();
            cmd4.Connection = conn;

            OracleDataAdapter adpt5 = new OracleDataAdapter();
            OracleCommand cmd5 = new OracleCommand();
            cmd5.Connection = conn;

            string kodepeg = ((FormParent)MdiParent).kode_accActive;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "SELECT * FROM soal where kode_pegawai='" + kodepeg + "' ORDER BY KODE_SOAL ASC";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "SELECT * FROM soal@pucukecabang where kode_pegawai='" + kodepeg + "' ORDER BY KODE_SOAL ASC";

            adpt.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            dataGridView1.DataSource = dt;
            
            cmd2 = new OracleCommand("SELECT * FROM KELAS", conn);
            OracleDataReader reader2 = cmd2.ExecuteReader();
            reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                listkode.Add(reader2["kode_kelas"].ToString());
                listmapel.Add(reader2["mata_pelajaran"].ToString());
                listgrade.Add(reader2["grade"].ToString());
                listjadwal.Add(reader2["kode_jadwal"].ToString());
            }

            cmd2 = new OracleCommand("SELECT * FROM KELAS@pucukecabang", conn);
            reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                listkode.Add(reader2["kode_kelas"].ToString());
                listmapel.Add(reader2["mata_pelajaran"].ToString());
                listgrade.Add(reader2["grade"].ToString());
                listjadwal.Add(reader2["kode_jadwal"].ToString());
            }
            if (((FormParent)MdiParent).db=="pusat") { cmd3 = new OracleCommand("SELECT * FROM soal", conn); }
            else if (((FormParent)MdiParent).db == "cabang") { cmd3 = new OracleCommand("SELECT * FROM soal@pucukecabang", conn); }

            OracleDataReader reader3 = cmd3.ExecuteReader();
            reader3 = cmd3.ExecuteReader();
            while (reader3.Read())
            {
                listsoal.Add(reader3["kode_soal"].ToString());
                listkelas.Add(reader3["kode_kelas"].ToString());
                listmateri.Add(reader3["judul_materi"].ToString());
                listpegawai.Add(reader3["kode_pegawai"].ToString());
            }
            string hasil = "";
            foreach (var item in listkode)
            {
                bool berhasil = true;
                foreach (var item2 in listkelas)
                {
                    if (item==item2)
                    {
                        berhasil = false;
                    }
                }
                if (berhasil)
                {
                    list_kelas_yang_kosong.Add(item);
                    if (hasil=="")
                    {
                        hasil = hasil + "a.kode_kelas='" +item +"'";
                    }
                    else
                    {
                        hasil = hasil + " or a.kode_kelas='" + item + "'";
                    }
                }
            }
            if (hasil != "")
            {
                cmd4.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas a, jadwal b where (" + hasil + ") and a.kode_jadwal = b.kode_jadwal";
                adpt4.SelectCommand = cmd4;
                DataTable dt4 = new DataTable();
                adpt4.Fill(dt4);

                cmd5.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas@pucukecabang a, jadwal b where (" + hasil + ") and a.kode_jadwal = b.kode_jadwal";
                adpt5.SelectCommand = cmd5;
                DataTable dt5 = new DataTable();
                adpt5.Fill(dt5);
                dt4.Merge(dt5);

                dataGridView2.DataSource = dt4;
            }
            else
            {
                DataTable dt4 = new DataTable();
                dataGridView2.DataSource = dt4;
            }

            conn.Close();
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)//BUTTON ADD SOAL
        {
            string judul = textBox1.Text;
            string kelas = textBox2.Text;

            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO soal VALUES('','" + kelas + "','" + judul + "','" + ((FormParent)MdiParent).kode_accActive + "')";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO soal@pucukecabang VALUES('','" + kelas + "','" + judul + "','" + ((FormParent)MdiParent).kode_accActive + "')";
            cmd.ExecuteNonQuery();
            refresh();
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)//DOUBLE CLICK KELAS KOSONG DIDGV
        {
            if (e.RowIndex!=-1)
            {
                textBox2.Text = dataGridView2[0,e.RowIndex].Value.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)//BUTTON BACK TO MENU PEGAWAI
        {
            ((FormParent)MdiParent).openPegawai();
            this.Close();
        }
    }
}
