﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class Form1 : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=sby;User Id=projectdd;password=123");
        string mapel = "ALL";
        

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            isiCB1();
            isiCB2();
        }

        public void isiCB1()
        {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from kelas", conn);
            if(mapel == "ALL")
            {
                cmd = new OracleCommand("select * from kelas", conn);
            }
            else
            {
                cmd = new OracleCommand("select * from kelas where mata_pelajaran='" + mapel + "'", conn);
            }

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "kelas");
            comboBox1.DataSource = ds.Tables["kelas"];
            comboBox1.DisplayMember = "kode_kelas";
            comboBox1.ValueMember = "kode_kelas";
            conn.Close();
            comboBox1.SelectedIndex = -1;
        }

        public void isiCB2()
        {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from soal", conn);
            if (mapel == "ALL")
            {
                if(radioButton1.Checked) cmd = new OracleCommand("select * from soal", conn);
                else if(radioButton2.Checked) cmd = new OracleCommand("select * from soal@PuCUKePUSAT", conn);
            }
            else
            {
                if (radioButton1.Checked) cmd = new OracleCommand("select * from soal where mata_kuliah='" + mapel + "'", conn);
                else if (radioButton2.Checked) cmd = new OracleCommand("select * from soal@PuCUKePUSAT where mata_kuliah='" + mapel + "'", conn);
            }
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "matapelajaran");
            comboBox2.DataSource = ds.Tables["matapelajaran"];
            comboBox2.DisplayMember = "kode_soal";
            comboBox2.ValueMember = "kode_soal";
            conn.Close();
            comboBox2.SelectedIndex = -1;
        }

        //filter database
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton1.Checked) conn = new OracleConnection("Data Source=sby;User Id=projectdd;password=123");
            else if(radioButton2.Checked) conn = new OracleConnection("Data Source=keedwin;User Id=projectdd;password=123");
            isiCB1();
            isiCB2();
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) conn = new OracleConnection("Data Source=sby;User Id=projectdd;password=123");
            else if (radioButton2.Checked) conn = new OracleConnection("Data Source=keedwin;User Id=projectdd;password=123");
            isiCB1();
            isiCB2();
        }

        //filter mapel
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) mapel = "MATEMATIKA";
            else if (radioButton4.Checked) mapel = "INGGRIS";
            else if (radioButton5.Checked) mapel = "ALL";
            isiCB1();
            isiCB2();
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) mapel = "MATEMATIKA";
            else if (radioButton4.Checked) mapel = "INGGRIS";
            else if (radioButton5.Checked) mapel = "ALL";
            isiCB1();
            isiCB2();
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) mapel = "MATEMATIKA";
            else if (radioButton4.Checked) mapel = "INGGRIS";
            else if (radioButton5.Checked) mapel = "ALL";
            isiCB1();
            isiCB2();
        }
    }
}
