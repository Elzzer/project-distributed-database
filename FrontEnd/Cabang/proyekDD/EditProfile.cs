﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class EditProfile : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=sby;User Id=projectdd;password=123");
        string passwordOld = "";
        public EditProfile()
        {
            InitializeComponent();
        }

        private void EditProfile_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            //LOAD SISWA
            textBox1.Text = cariData("email");
            textBox2.Text = cariData("nama");
            passwordOld = cariData("password");
        }

        public string cariData(string cari) {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from siswa", conn);
            if (cari == "email") {
                if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select email from siswa where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
                else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select email from siswa@pucukecabang where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
            }
            if (cari == "nama")
            {
                if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select nama from siswa where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
                else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select nama from siswa@pucukecabang where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
            }
            if (cari == "password")
            {
                if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select password from siswa where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
                else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select password from siswa@pucukecabang where kode_siswa = '" + ((FormParent)MdiParent).kode_accActive + "'", conn);
            }
            string hasil = cmd.ExecuteScalar().ToString();

            conn.Close();

            return hasil;
        }

        public void refresh() {
            ((FormParent)MdiParent).openEditProfile();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ((FormParent)MdiParent).openSiswa();
            this.Close();
        }//BACK TO MAIN MENU SISWA
        private void button2_Click(object sender, EventArgs e)//BUTTON UPDATE PROFILE
        {
            if (textBox4.Text == textBox5.Text) {
                if (textBox3.Text == passwordOld) {
                    conn.Open();
                    OracleCommand cmd=new OracleCommand();
                    if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("update siswa set nama='" + textBox2.Text + "', password='" + textBox4.Text + "' where email='" + textBox1.Text + "'", conn);
                    else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("update siswa@pucukecabang set nama='" + textBox2.Text + "', password='" + textBox4.Text + "' where email='" + textBox1.Text + "'", conn);
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    MessageBox.Show("Updated!!!");
                    refresh();
                }
                else MessageBox.Show("Old Password Wrong!!!");
            }
            else MessageBox.Show("Password And Confirm Password Must Same!!!");
        }
    }
}
