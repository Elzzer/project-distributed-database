﻿using System.Data;

namespace proyekDD
{
    partial class PPLihatAbsen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonLihatAbsenBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonAbsenEmail;
        private System.Windows.Forms.RadioButton radioButtonAbsenNama;
        private System.Windows.Forms.RadioButton radioButtonAbsenID;
        private System.Windows.Forms.Button buttonAbsenSearch;
        private System.Windows.Forms.TextBox textBoxAbsenField;
    }
}

