﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyekDD
{
    public partial class FormParent : Form
    {
        public string kode_accActive = "";
        public string nama_accActive = "";
        public string tipe_accActive = "";
        public string lokasi_accActive = "";
        public string db = "pusat";

        public FormParent()
        {
            InitializeComponent();
        }

        private void FormParent_Load(object sender, EventArgs e)
        {
            openLogin();
        }

        public void openLogin() {
            Login l = new Login();
            l.MdiParent = this;
            l.Show();
        }

        public void openPegawai() {
            Pegawai l = new Pegawai();
            l.MdiParent = this;
            l.Show();
        }

        public void openAddSiswa() {
            InsertSiswa l = new InsertSiswa();
            l.MdiParent = this;
            l.Show();
        }

        public void openAbsentPegawai() {
            PPLihatAbsen l = new PPLihatAbsen();
            l.MdiParent = this;
            l.Show();
        }

        public void openHistoryPembelajaran() {
            HistoryPembelajaran l = new HistoryPembelajaran();
            l.MdiParent = this;
            l.Show();
        }

        public void openInsertSoal() {
            InsertSoal l = new InsertSoal();
            l.MdiParent = this;
            l.Show();
        }

        public void openKelas()
        {
            InsertKelas l = new InsertKelas();
            l.MdiParent = this;
            l.Show();
        }

        public void openSiswa() {
            Siswa l = new Siswa();
            l.MdiParent = this;
            l.Show();
        }

        public void openEditProfile() {
            EditProfile l = new EditProfile();
            l.MdiParent = this;
            l.Show();
        }

        public void openIkutKelas() {
            IkutKelas l = new IkutKelas();
            l.MdiParent = this;
            l.Show();
        }
    }
}
