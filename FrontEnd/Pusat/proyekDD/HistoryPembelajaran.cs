﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;


namespace proyekDD
{
    public partial class HistoryPembelajaran : Form
    {
        OracleConnection conn;
        String username = "projectdd";
        String password = "123";
        String DB = "SBY";
        String oradb;

        //data
        String id;
        String nilai;
        String siswa;
        String kelas;
        String soal;
        String pegawai;
        int counter;

        public HistoryPembelajaran()
        {
            InitializeComponent();
        }
        
        private void HistoryPembelajaran_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            oradb = "Data Source=" + DB + ";User ID=" + username + ";Password=" + password;
            pegawai = ((FormParent)MdiParent).kode_accActive;
            refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nilai = tbgrade.Value.ToString();
            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "UPDATE history_siswa SET kode_pegawai = '"+pegawai+"' , nilai = '"+ nilai +"' , status='complete' where id='"+id+"'";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "UPDATE history_siswa@pucukecabang SET kode_pegawai = '" + pegawai + "' , nilai = '" + nilai + "' , status='complete' where id='" + id + "'";
            cmd.ExecuteNonQuery();

            //-------------------
            int ctrCariKelas = 0; string lokasiKelas = "disini";
            cmd.CommandText = "select count(*) from kelas where kode_kelas='" + kelas + "'";
            ctrCariKelas = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            if (ctrCariKelas == 0) lokasiKelas = "disana";
            
            if (lokasiKelas == "disini") cmd.CommandText = "select mata_pelajaran from kelas where kode_kelas='" + kelas + "'";
            else if (lokasiKelas == "disana") cmd.CommandText = "select mata_pelajaran from kelas@pucukecabang where kode_kelas='" + kelas + "'";
            string matapelajaran = cmd.ExecuteScalar().ToString();

            //MessageBox.Show(matapelajaran);

            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "select grade from pelajaran where kode_siswa='"+siswa+"' and mata_pelajaran ='"+matapelajaran+"'";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "select grade from pelajaran@pucukecabang where kode_siswa='" + siswa + "' and mata_pelajaran ='" + matapelajaran + "'";
            string grade = cmd.ExecuteScalar().ToString();

            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "select ctrles from pelajaran where kode_siswa='" + siswa + "' and mata_pelajaran ='" + matapelajaran + "'";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "select ctrles from pelajaran@pucukecabang where kode_siswa='" + siswa + "' and mata_pelajaran ='" + matapelajaran + "'";
            counter = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            if(Convert.ToInt32(nilai) > 70)counter++; //CEK BATAS REMIDI
            char[] gradeTemp = grade.ToCharArray();
            if (counter == 5)
            {
                counter = 0;
                gradeTemp[0]++;
                MessageBox.Show("Naik Grade ke -> " + gradeTemp[0]);
            }
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "update pelajaran set ctrles = '" + counter.ToString() + "', grade='" + gradeTemp[0].ToString() +"' where kode_siswa='" + siswa + "' and mata_pelajaran ='" + matapelajaran + "'";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "update pelajaran@pucukecabang set ctrles = '" + counter.ToString() + "', grade='" + gradeTemp[0].ToString() + "' where kode_siswa='" + siswa + "' and mata_pelajaran ='" + matapelajaran + "'";
            cmd.ExecuteNonQuery();

            refresh();
        }
        public void refresh()
        {
            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "SELECT * FROM history_siswa where status = 'pending' ORDER BY ID ASC";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "SELECT * FROM history_siswa@pucukecabang where status = 'pending' ORDER BY ID ASC";
            adpt.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            dataGridView1.DataSource = dt;

            OracleCommand cmd2 = new OracleCommand();
            cmd2.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd2.CommandText = "SELECT * FROM history_siswa where status = 'complete' and kode_pegawai = '"+ pegawai +"' ORDER BY KODE_SISWA ASC";
            else if (((FormParent)MdiParent).db == "cabang") cmd2.CommandText = "SELECT * FROM history_siswa@pucukecabang where status = 'complete' and kode_pegawai = '" + pegawai + "' ORDER BY KODE_SISWA ASC";
            adpt.SelectCommand = cmd2;
            DataTable dt2 = new DataTable();
            adpt.Fill(dt2);
            dataGridView2.DataSource = dt2;
            conn.Close();
            tbgrade.Value=0;
            tbsiswa.Clear();
        }

        private void button2_Click(object sender, EventArgs e)//BACK TO MENU
        {
            ((FormParent)MdiParent).openPegawai();
            this.Close();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id = dataGridView1[0, e.RowIndex].Value.ToString();
            siswa = dataGridView1[2, e.RowIndex].Value.ToString();
            kelas = dataGridView1[3, e.RowIndex].Value.ToString();
            soal = dataGridView1[4, e.RowIndex].Value.ToString();
            tbsiswa.Text = dataGridView1[2, e.RowIndex].Value.ToString();
        }
    }
}
