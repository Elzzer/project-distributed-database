﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class PPLihatAbsen : Form
    {
        OracleConnection conn;
        String username = "projectdd";
        String password = "123";
        String DB = "SBY";
        String oradb;
        public PPLihatAbsen()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonLihatAbsenBack = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonAbsenSearch = new System.Windows.Forms.Button();
            this.textBoxAbsenField = new System.Windows.Forms.TextBox();
            this.radioButtonAbsenID = new System.Windows.Forms.RadioButton();
            this.radioButtonAbsenEmail = new System.Windows.Forms.RadioButton();
            this.radioButtonAbsenNama = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 116);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(725, 275);
            this.dataGridView2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "Lihat Absensi Siswa";
            // 
            // buttonLihatAbsenBack
            // 
            this.buttonLihatAbsenBack.Location = new System.Drawing.Point(12, 12);
            this.buttonLihatAbsenBack.Name = "buttonLihatAbsenBack";
            this.buttonLihatAbsenBack.Size = new System.Drawing.Size(78, 40);
            this.buttonLihatAbsenBack.TabIndex = 3;
            this.buttonLihatAbsenBack.Text = "Back";
            this.buttonLihatAbsenBack.UseVisualStyleBackColor = true;
            this.buttonLihatAbsenBack.Click += new System.EventHandler(this.ButtonLihatAbsenBack_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cek berdasarkan:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonAbsenSearch);
            this.groupBox1.Controls.Add(this.textBoxAbsenField);
            this.groupBox1.Controls.Add(this.radioButtonAbsenID);
            this.groupBox1.Controls.Add(this.radioButtonAbsenEmail);
            this.groupBox1.Controls.Add(this.radioButtonAbsenNama);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 413);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 215);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Absen Siswa";
            // 
            // buttonAbsenSearch
            // 
            this.buttonAbsenSearch.Location = new System.Drawing.Point(24, 143);
            this.buttonAbsenSearch.Name = "buttonAbsenSearch";
            this.buttonAbsenSearch.Size = new System.Drawing.Size(289, 43);
            this.buttonAbsenSearch.TabIndex = 9;
            this.buttonAbsenSearch.Text = "Search";
            this.buttonAbsenSearch.UseVisualStyleBackColor = true;
            this.buttonAbsenSearch.Click += new System.EventHandler(this.ButtonAbsenSearch_Click);
            // 
            // textBoxAbsenField
            // 
            this.textBoxAbsenField.Location = new System.Drawing.Point(24, 105);
            this.textBoxAbsenField.Name = "textBoxAbsenField";
            this.textBoxAbsenField.Size = new System.Drawing.Size(289, 22);
            this.textBoxAbsenField.TabIndex = 8;
            // 
            // radioButtonAbsenID
            // 
            this.radioButtonAbsenID.AutoSize = true;
            this.radioButtonAbsenID.Location = new System.Drawing.Point(165, 68);
            this.radioButtonAbsenID.Name = "radioButtonAbsenID";
            this.radioButtonAbsenID.Size = new System.Drawing.Size(102, 21);
            this.radioButtonAbsenID.TabIndex = 7;
            this.radioButtonAbsenID.TabStop = true;
            this.radioButtonAbsenID.Text = "Kode Siswa";
            this.radioButtonAbsenID.UseVisualStyleBackColor = true;
            // 
            // radioButtonAbsenEmail
            // 
            this.radioButtonAbsenEmail.AutoSize = true;
            this.radioButtonAbsenEmail.Location = new System.Drawing.Point(96, 68);
            this.radioButtonAbsenEmail.Name = "radioButtonAbsenEmail";
            this.radioButtonAbsenEmail.Size = new System.Drawing.Size(63, 21);
            this.radioButtonAbsenEmail.TabIndex = 6;
            this.radioButtonAbsenEmail.TabStop = true;
            this.radioButtonAbsenEmail.Text = "Email";
            this.radioButtonAbsenEmail.UseVisualStyleBackColor = true;
            // 
            // radioButtonAbsenNama
            // 
            this.radioButtonAbsenNama.AutoSize = true;
            this.radioButtonAbsenNama.Location = new System.Drawing.Point(24, 68);
            this.radioButtonAbsenNama.Name = "radioButtonAbsenNama";
            this.radioButtonAbsenNama.Size = new System.Drawing.Size(66, 21);
            this.radioButtonAbsenNama.TabIndex = 5;
            this.radioButtonAbsenNama.TabStop = true;
            this.radioButtonAbsenNama.Text = "Nama";
            this.radioButtonAbsenNama.UseVisualStyleBackColor = true;
            // 
            // PPLihatAbsen
            // 
            this.ClientSize = new System.Drawing.Size(755, 636);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonLihatAbsenBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PPLihatAbsen";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            oradb = "Data Source=" + DB + ";User ID="+username+";Password="+password; 
            refresh("");
            radioButtonAbsenNama.Checked = true;
        }
        
        public void refresh(String where)
        {
            String query = "";
            if (((FormParent)MdiParent).db=="pusat")query = "SELECT AB.KODE_SISWA, S.NAMA, S.EMAIL, AB.TGL, AB.MATA_PELAJARAN FROM LOG_ABSENSI AB" +" INNER JOIN SISWA S ON S.KODE_SISWA = AB.KODE_SISWA"+where;
            else if (((FormParent)MdiParent).db == "cabang") query = "SELECT AB.KODE_SISWA, S.NAMA, S.EMAIL, AB.TGL, AB.MATA_PELAJARAN FROM LOG_ABSENSI@pucukecabang AB" + " INNER JOIN SISWA@pucukecabang S ON S.KODE_SISWA = AB.KODE_SISWA" + where;
            
            // ada di owner@sby/owner
            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = query;
            adpt.SelectCommand = cmd;
            DataTable dt2 = new DataTable();
            adpt.Fill(dt2);
            dataGridView2.DataSource = dt2;
            conn.Close();
        }

        private void ButtonAbsenSearch_Click(object sender, EventArgs e)
        {
            String query = textBoxAbsenField.Text.ToString();
            String where = " WHERE ";
            if (radioButtonAbsenEmail.Checked == true) {
                where += "LOWER(S.EMAIL) LIKE '%" + query.ToLower()+"%'";
            }
            else if (radioButtonAbsenID.Checked == true)
            {
                where += "LOWER(S.KODE_SISWA) LIKE '%" + query.ToLower() + "%'";
            }
            else
            {
                where += "LOWER(S.NAMA) LIKE '%" + query.ToLower() + "%'";
            }
            refresh(where);
        }

        private void ButtonLihatAbsenBack_Click(object sender, EventArgs e)
        {
            //balik ke pegawai cabang home
            ((FormParent)MdiParent).openPegawai();
            this.Close();
        }
    }
}
