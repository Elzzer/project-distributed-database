(ORCL)

--give previllage and create user
create user projectddmv identified by 123;
grant create materialized view to projectddmv;
grant connect to projectddmv;
grant create database link to projectddmv;
grant create public database link to projectddmv;
grant resource to projectddmv;
grant create job to projectddmv;

--create database link
create database link PuFUKeferdi connect to projectDD identified by "123" using 'KeFerdi';
create database link PuFUKeedwin connect to projectDD identified by "123" using 'KeEdwin';

--create materialized view

--siswa
create materialized view mvSiswaPusat as
select * from siswa@PuFUKeferdi 
order by kode_siswa;

--pegawai
create materialized view mvPegawaiPusat as
select * from pegawai@PuFUKeferdi 
order by kode_pegawai;
create materialized view mvPegawaiEdwin as
select * from pegawai@PuFUKeedwin 
order by kode_pegawai;

--soal
create materialized view mvSoalPusat as
select * from soal@PuFUKeferdi 
order by kode_soal;

--log absensi
create materialized view mvlog_absensiPusat as
select * from log_absensi@PuFUKeferdi;


--kelas
create materialized view mvKelasPusat as
select * from kelas@PuFUKeferdi 
order by kode_kelas;
create materialized view mvKelasEdwin as
select * from kelas@PuFUKeedwin  
order by kode_kelas;

--pelajaran
create materialized view mvPelajaranPusat as
select * from pelajaran@PuFUKeferdi;

--jadwal
create materialized view mvJadwalPusat as
select * from jadwal@PuFUKeferdi;
create materialized view mvJadwalEdwin as
select * from jadwal@PuFUKeedwin;

--history siswa
create materialized view mvhistory_siswaPusat as
select * from history_siswa@PuFUKeferdi;

--mata pelajaran
create materialized view mvmata_pelajaranPusat as
select * from mata_pelajaran@PuFUKeferdi;


--execute dbms_mview.refresh('mvlog_absensiPusat');


--create procedure untuk temp (semua refresh)
CREATE OR REPLACE PROCEDURE refresh_data
AS
BEGIN    	
 	DBMS_MVIEW.REFRESH('mvSiswaPusat');
    	DBMS_MVIEW.REFRESH('mvPegawaiPusat');
    	DBMS_MVIEW.REFRESH('mvSoalPusat');
    	DBMS_MVIEW.REFRESH('mvlog_absensiPusat');
    	DBMS_MVIEW.REFRESH('mvKelasPusat');
    	DBMS_MVIEW.REFRESH('mvPelajaranPusat');
    	DBMS_MVIEW.REFRESH('mvJadwalPusat');
    	DBMS_MVIEW.REFRESH('mvhistory_siswaPusat');
    	DBMS_MVIEW.REFRESH('mvmata_pelajaranPusat');
END;
/


CREATE OR REPLACE PROCEDURE refresh_data_edwin
AS
BEGIN
    	DBMS_MVIEW.REFRESH('mvPegawaiEdwin');
    	DBMS_MVIEW.REFRESH('mvKelasEdwin');
    	DBMS_MVIEW.REFRESH('mvJadwalEdwin');
END;
/


--CREATE schedule refresh mv
BEGIN 
DBMS_SCHEDULER.CREATE_JOB ( 
	job_name => 'update_data_job', 
	job_type => 'STORED_PROCEDURE', 
	job_action => 'refresh_data', 
	repeat_interval => 'FREQ=MINUTELY; INTERVAL=1;',
	enabled => TRUE
); 
END;
/
BEGIN 
DBMS_SCHEDULER.CREATE_JOB ( 
	job_name => 'update_data_job_edwin', 
	job_type => 'STORED_PROCEDURE', 
	job_action => 'refresh_data_edwin', 
	repeat_interval => 'FREQ=MINUTELY; INTERVAL=1;',
	enabled => TRUE
); 
END;
/

--DROP SCHEDULE MV
BEGIN
  DBMS_SCHEDULER.DROP_JOB ('update_data_job');
END;
/
BEGIN
  DBMS_SCHEDULER.DROP_JOB ('update_data_job_edwin');
END;
/

